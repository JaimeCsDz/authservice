import type IUser from '@/interface/IUser'
import { ref } from 'vue'
import type { Ref } from 'vue'



export default class AuthService{
    private users: Ref<IUser[]>
    private user: Ref<IUser>
    private api = "https://utcancun.a.pinggy.online"

    constructor(){
        this.users = ref([]) 
        this.user = ref ({}) as Ref<IUser>
    }

    getUsers(): Ref<IUser[]>{
        return this.users
    }
    getUser(): Ref<IUser>{
        return this.user
    }
    //Funcion para traer todos los usuarios
    async fetchUsers(): Promise<void> {
        try {
            const json = await fetch(`${this.api}/users`)
            const response = await json.json()
            this.users.value = await response
        } catch (error) {
            console.log(error)
        }
    }
    //traer solamente un valor o un usuario seleccionado
        async fetchUser(email: string, name: string, group: string): Promise<void> {
        try {
            const json = await fetch(`${this.api}/user?email=${email}&name=${name}&group=${group}`)
            const response = await json.json()
            
            this.user.value = await response
        } catch (error) {
            console.log(error)
        }
    }
    
    async fetchRegister(name: string, email: string, password:string, group:string): Promise<void> {
        try{
            const formData = new FormData()
            formData.append('Nombre', name),
            formData.append('Correo',email),
            formData.append('Contra',password)
            formData.append('Grupo', group)
            const respuesta = await fetch(`${this.api}/register`,{
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                body:formData
            })
            if(respuesta.ok){
                alert("Usuario registrado")
            }else{
                console.log("error", respuesta.status)
            }
            console.log(respuesta)
            }catch (error){
                console.log("error en la solicitud", error)
            }
    }
}